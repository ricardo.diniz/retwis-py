# retwis-py

Adaptation from [Python Twitter clone](https://github.com/pims/retwis-py/) to run on flask

Follows official tutorial from [Python Twitter Clone on REDIS](https://redis.io/topics/twitter-clone)

# Running

Run app directly on flask

```
PYTHONPATH=. FLASK_APP=retwis FLASK_ENV=development flask run
```


from retwis.redis.base import Model
from retwis import redis


class User(Model):
    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)

    @staticmethod
    def find_by_username(username, r):
        _id = r.get("user:username:%s" % username)
        if _id is not None:
            return User(int(_id), db=r)
        else:
            return None

    @staticmethod
    def find_by_id(_id, r):
        if r.exists("user:id:%s:username" % _id):
            return User(int(_id), db=r)
        else:
            return None

    @staticmethod
    def create(username, password, r):
        user_id = r.incr("user:uid")
        if not r.get("user:username:%s" % username):
            r.set("user:id:%s:username" % user_id, username)
            r.set("user:username:%s" % username, user_id)

            r.set("user:id:%s:password" % user_id, password)
            r.lpush("users", user_id)
            return User(user_id, db=r)
        return None

    def posts(self, page=1,):
        pass

    def timeline(self, page=1):
        pass

    def mentions(self, page=1):
        pass

    def add_post(self, post, r):
        pass

    def add_timeline_post(self, post):
        pass

    def add_mention(self, post):
        pass

    def follow(self, user):
        pass

    def stop_following(self, user):
        pass

    def following(self, user):
        pass

    @property
    def followers(self):
        pass

    @property
    def followees(self):
        pass

    # added
    @property
    def tweet_count(self):
        pass

    @property
    def followees_count(self):
        pass

    @property
    def followers_count(self):
        pass

    def add_follower(self, user):
        pass

    def remove_follower(self, user):
        pass

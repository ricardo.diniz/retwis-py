import redis

import click
from flask import current_app, g
from flask.cli import with_appcontext


def get_db():
    db_type = current_app.config['DATABASE']

    if 'db' not in g:
        if db_type == 'redis':
            g.db = redis.Redis(
                host=current_app.config['DATABASE_HOST'],
                port=current_app.config['DATABASE_PORT'],
                db=current_app.config['DATABASE_NAME'])

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()


def init_db():
    db = get_db()

    # Just execute startup script if supplied. Most NoSQL databases don't have it
    if current_app.config.get('DATABASE_SCRIPT'):
        with current_app.open_resource(current_app.config['DATABASE_SCRIPT']) as f:
            db.executescript(f.read().decode('utf8'))


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
